# Exercício Aula 1 Ruby

# Crie uma função que dado um array de arrays, imprima na tela a soma e a multiplicação de todos os valores. 
# Ex: Entrada: [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
#     Impressão na tela: Soma: 39
#     Impressão na tela: Multiplicação: 100800

def soma_e_multiplica (arrs)
    #Declara as variáveis "soma" e "mult".
    soma = 0
    mult = 1
    #Percorre a array externa selecionando cada array.
    arrs.each do |arr| 
        #Percorre a array interna selecionando cada número.
        arr.each do |num|
            #Atribui a "soma" seu valor somado ao enésimo número.
            soma += num
            #Atribui a "mult" seu valor multiplicado ao enésimo número.
            mult *= num
        end
    end
    #Imprime o resultado
    puts("Soma: #{soma}")
    puts("Multiplicação: #{mult}")
end
    

#soma_e_multiplica([[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]])


# Crie uma função que dado dois valores, retorne a divisão deles (a divisão deve retornar um float).
# Ex: Entrada: 5, 2
#     Saída: 2.5

def divide(a, b)
    #Retorna a divisão de "a" convertido para float por "b" convertido para float. 
    return a.to_f / b.to_f 
end

#puts(divide(5, 2))


# Crie uma função, que dado um hash como parâmetro, e retorne um array com todos os valores que estão no hash elevados ao quadrado.
# Ex: Entrada: {:chave1 => 5, :chave2 => 30, :chave3 => 20}
#     Saída: [25, 900, 400]

def quadrado(hash)
    #Cria array vazia.
    arr = []
    #Para cada elemento do hash, seleciona a chave e seu valor.
    hash.each do |key, value|
        #Eleva p enésimo valor ao quadrado e o adiciona à array. 
        arr.push(value ** 2)
    end
    #Retorna a array com os valores elevados ao quadrado.
    return arr
end

#print(quadrado({:chave1 => 5, :chave2 => 30, :chave3 => 20}))
        

# Crie uma função que dado um array de inteiros, faça um Cast para String e retorne um array com os valores em string.
# Ex: Entrada: [25, 35, 45]
#     Saída: ["25", "35", "45"]

def retorna_strig(arr)
    #Cria array vazia.
    arr_s = []
    #Seleciona cada número da array.
    arr.each do |n|
        #Adiciona à "arr_s" o enésimo número convertido para string.
        arr_s.push(n.to_s)
    end
    #Retorna a "arr_s" com os valores convertidos para string.
    return arr_s
end

#print(retorna_strig([25, 35, 45]))


# Crie uma função que dado um array de inteiros, retorne um array apenas com os divisíveis por 3.
# Ex: Entrada: [3, 6, 7, 8]
#     Saída: [3, 6]

def multiplos_de_3(arr)
    #Cria array vazia.
    arr_s = []
    #Para cada número na array, executa o bloco abaixo.
    arr.each do |n|
        #Se o número for diviível por 3, adiciona-o à array. 
        if (n % 3 == 0) then arr_s.push(n)
        end
    end
    #Retorna a array contendo apenas número divisíveis por 3.
    return arr_s
end

#print(multiplos_de_3([3, 6, 7, 8]))